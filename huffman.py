'''
Created on 8 Dec. 2017

@author: andenix
'''

import struct
import functools

__all__=['encode', 'huffman_compressed']


bit_table = tuple(map(lambda s: int(s, 16), line.strip().split(None, 1)) for line in open("bit_table.txt") if line.rstrip())
   
pack_word_into = struct.Struct('>H').pack_into
pack_dword_into = struct.Struct('>I').pack_into


def huffman_compressed(f):    
    @functools.wraps(f)
    def wrapper(*args, **kwds):
        if isinstance(args[1], basestring):
            args[1]=encode(args[1])
        else:
            args[0]=encode(args[0])             
        return f(*args, **kwds)
    return wrapper


def encode(s):
    cbuff = bytearray()
    b_offset = 0    
    pval = bytearray(4)
    for c in s:                        
        blen, bval = bit_table[ord(c)]
        bytes_needed = blen-8+b_offset
        if bytes_needed<=0:
            bytes_needed = 1
        elif bytes_needed<=8:
            bytes_needed = 2                    
        else:
            bytes_needed = 3                   
            
        pack_word_into(pval, 0, bval << (16-blen-b_offset)) if (bytes_needed<3) else pack_dword_into(pval, 0, bval << (32-blen-b_offset))                
                    
        if not (b_offset%8):
            cbuff.extend(pval[:bytes_needed])
        else:
            cbuff[-1] |= pval[0]            
            cbuff.extend(pval[1:bytes_needed])
        
        b_offset = (b_offset + blen)%8        

    if not (b_offset%8):
        cbuff.append(0b1101<<4)            
    elif b_offset<5:
        cbuff[-1] |= (0b1101<<(4-b_offset))        
    else:
        pack_word_into(pval, 0, 0b1101 << (12-b_offset))
        cbuff[-1] |= pval[0]
        cbuff.extend(pval[1:2])                                        
    return cbuff


if __name__ == '__main__':        
    from timeit import default_timer as clock
    when = clock()
    data = "axrsd jds hds x1 sdsada d2 ezxzcx 21wsasa1 sdd a sada112 2 31"    
    print "data len:", len(data), "encoded:", len(encode(data))
    for _ in xrange(10000):
        encode(data)
    print 'done in %.4f' % (clock()-when)
            