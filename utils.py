import logging
import os
import socket
import sys


__all__ = ['get_ip', 'get_logger', 'hexlifyPacket', 'hex_logged', 'public']



def hex_logged(prefix='', max_chars=None, logger=None):
    def real_decorator(f):        
        def wrapper(*args, **kwargs):
            lgr=args[0].logger if logger is None else logger
            if len(args)>1:
                lgr.info(prefix + hexlifyPacket(args[1], max_chars-len(prefix) if max_chars is not None else None))
            else:
                lgr.info(prefix + hexlifyPacket(args[0].data, max_chars-len(prefix) if max_chars is not None else None))
            f(*args, **kwargs)    
        return wrapper    
    return real_decorator
    

def hexlifyPacket(packet_data, max_chars = None):   
    import binascii 
    packet_hex = binascii.hexlify(packet_data).upper()    
    s = " ".join([packet_hex[n:n+2] for n in range(0, len(packet_hex), 2)])
    if max_chars is not None:
        s = s[:max_chars]
    return s


def public(obj):
    _all = sys.modules[obj.__module__].__dict__.setdefault('__all__', [])  # @UndefinedVariable
    if obj.__name__ not in _all:  # Prevent duplicates if run from an IDE.
        _all.append(obj.__name__)
    return obj


def get_ip():    
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP


def get_logger(name=None, filename=None):
    logger = logging.getLogger(name)                
    logger.setLevel(logging.DEBUG)
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.INFO)  
                                          
    con_f_fmt = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s', 
                            datefmt="%b %dT%H:%M:%S")            
    console_handler.setFormatter(con_f_fmt)           
    logger.addHandler(console_handler)
    
    log_filename = os.path.splitext(os.path.basename(sys.modules['__main__'].__file__))[0]+".log" if filename is None else filename
    file_handler = logging.FileHandler(log_filename)
    file_handler.setLevel(logging.DEBUG)
    file_h_fmt = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s', 
                            datefmt="%Y/%m/%d %H:%M:%S")
    file_handler.setFormatter(file_h_fmt)
    logger.addHandler(file_handler)            
    return logger
