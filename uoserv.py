#!/usr/bin/env python2
# coding: Windows-1251

'''
Created on 28 Nov, 2017

@author: andenix
'''

# at windows please perform: pip2 install pypiwin32

import hashlib
import os
import socket
import struct
import sys

from twisted.internet import reactor, task, defer
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet.error import CannotListenError
from twisted.internet.protocol import Protocol
from twisted.internet.protocol import ServerFactory

from packets import *
from ruamel.yaml import YAML, yaml_object
from utils import *


UOSERV_VER = '0.2E.9 "Devola&Popola"'
DEFAULT_PORT = 2593

SERVER_DIR = os.path.dirname(__file__)
WORLD_SAVE_DIR = u'worldstate'

uoserv_log = get_logger()

yaml = YAML()

conf = yaml.load(open(os.path.join(SERVER_DIR, 'settings.yaml')))


@yaml_object(yaml)
class Account(object):    
    yaml_tag = u'!Account'
    def __init__(self, name, password):
        self.name = name
        self._passwordHash = hashlib.md5(name+password).hexdigest()
        
    def checkPassword(self, password):
        if hasattr(self, 'password'):
            self._passwordHash = hashlib.md5(self.name+self.password).hexdigest()
            del self.password
            
        return hashlib.md5(self.name+password).hexdigest()==self._passwordHash
        
    def __str__(self):
        return "<{self.__class__.__name__}: {self.name}>".format(**vars())
    def __repr__(self):
        return "'{self.__class__.__name__}(\"{self.name}\")'".format(**vars())        


try:
    PORT = conf['server']['port']
except KeyError:
    PORT = DEFAULT_PORT
    uoserv_log.warn("No port defined in %s; defaulting to %d ..", 'settings.yaml', DEFAULT_PORT)
    
    
class UOProtocol(Protocol, object): # must be a new-style object to work
    def __init__(self, factory):
        super(UOProtocol, self).__init__()
        self._factory = factory
        
    def dataReceived(self, data):        
        try:
            self._buffer += data
        except AttributeError:
            self._buffer = data        
            
    def set_logger(self, logger):
        self._logger = logger
    
    @property
    def factory(self):
        return self._factory

    @property
    def data(self):
        return self._buffer
    
    @data.setter
    def data(self, new_data):
        self._buffer = new_data
    
    @property    
    def logger(self):
        try:
            return self._logger
        except AttributeError:
            return None


class UOProtoDemultiplexer(UOProtocol):
    def __init__(self, factory):
        super(UOProtoDemultiplexer, self).__init__(factory)
        
    @hex_logged('demuxer: ', max_chars=79)
    def dataReceived(self, *args, **kwargs):        
        try:
            self.effectiveProto.dataReceived(*args, **kwargs)      
        except AttributeError as e:
            if not hasattr(self, "effectiveProto"):            
                super(UOProtoDemultiplexer, self).dataReceived(*args, **kwargs)
            else:
                raise e
        else:
            return
                            
        try:
            pktID1, pktID2 = struct.unpack_from('BxxxB', self.data) 
        except struct.error:
            return
        
        if pktID2==LoginRequestPktIn.id:
            self.effectiveProto = UOLoginProtocol(self.factory)
            self.effectiveProto.set_logger(self.logger)
            self.effectiveProto.makeConnection(self.transport)
            self.effectiveProto.dataReceived(self.data)       
            self.data=""
            
    def connectionLost(self, reason):
        try:
            reason = self.effectiveProto.connectionLost(reason)
        except AttributeError:
            pass               
            
# -- game server --

class UOGameProtocol(UOProtocol):
    def __init__(self, factory):
        super(UOGameProtocol, self).__init__(factory)
        #self._init_tables()                 
        #self.state = self.STATE_HANDSHAKE       

# -- login server --


class UOLoginProtocol(UOProtocol):
    (
    STATE_HANDSHAKE,
    STATE_LOGIN,
    STATE_GAME,
    ) = range(1, 4)
    
    
    @classmethod            
    def _load_servers(cls):
        with open(os.path.join(SERVER_DIR, 'servers.yaml')) as servers_fp:
            cls.servers = yaml.load(servers_fp)
        return cls.servers        
    
    
    def __init__(self, factory):
        super(UOLoginProtocol, self).__init__(factory)
        self._init_tables()                 
        self.state = self.STATE_HANDSHAKE   
        #print yaml.dump(json.load(open('servers.json')), default_flow_style=False, indent=2, Dumper=yaml.CDumper)
            
    
    def _init_tables(self):
        self.hf_table = {getattr(self, k) : getattr(self, 'handle_%s' % k) for k in dir(self) if k.startswith('STATE_') and getattr(self, 'handle_%s' % k, None) is not None}                
        #self.pf_table = {getattr(self, k) : getattr(self, 'recv_%s' % k) for k in dir(self) if k.startswith('STATE_') and getattr(self, 'handle_%s' % k, None) is not None}
    
        
    def connectionMade(self):
                        
        self.numClients +=1
        
        uoserv_log.info("Client connected from %s, clients: %d", 
                        self.transport.getPeer().host, 
                        self.numClients)
        
        
    def connectionLost(self, reason):
        self.numClients -= 1
        self.logger.info("Disconnected from %s, clients: %d", 
                         self.transport.getPeer().host, self.numClients)
    
    @hex_logged('data recv: ')
    def dataReceived(self, data):
        super(UOLoginProtocol, self).dataReceived(data)
                
        data_handle_f = self.hf_table.get(self.state, None)
        if data_handle_f is not None:
            data_handle_f()
        else:        
            # � �� ���� ��� ����� ��� ����������, ������ ��� ������ ������ ������ � ������...
            (state_name,) = [k for k, v in self.__class__.__dict__.iteritems() if v is self.state]
            self.logger.warn("No handler for current state: %s", state_name)         
            
            
    def handle_STATE_LOGIN(self):
        (pck_in,) = UOPacket.getbyte_from(self.data)
        self.logger.info('MsgIn: 0x%x - %s', pck_in, PacketMsgIn(pck_in).name)
        
        try:
            recv_f = getattr(self, "recv_%s" % hex(pck_in))            
        except AttributeError:
            raise AttributeError('No handler for packed %s, %s' % hex(pck_in))
        
        recv_f() 
        
        if len(self.data): self.dataReceived(b'')
                    
    
    def handle_STATE_HANDSHAKE(self):
        if len(self.data)>=4:
            self._encrytion_key, self.data  = self.data[0:4], self.data[4:]
            self.state = self.STATE_LOGIN
            if len(self.data):
                self.dataReceived(b'')


    def send_game_servers(self):
        """Show game server list to a player"""
                
        try:
            servers = self.servers
        except AttributeError:
            servers = self.__class__._load_servers()
            
        GameServerListPktOut.send(self.transport, servers)

    
    def auth_player(self, account_name, password):
        """�������� ������; Player authentication"""
        try:
            return accounts[account_name.strip().lower()].checkPassword(password.strip())
        except KeyError as e:
            self.logger.info("Login error: %s", e)
            self.transport.write(LoginDeniedPktOut.build(LoginDeniedReason.InvalidAccountCredentials))
        return False
    
    def login_player(self, loginKey):

        self.send_game_servers()        
        
        #self.transport.write(LoginDeniedPktOut.build(LoginDeniedReason.AccountBlocked))


    def recv_0x80(self):     
        try:
            loginRequest = LoginRequestPktIn._unpack_ntuple(self.data)
        except struct.error:
            return
        else:        
            self.data = self.data[LoginRequestPktIn.size:]
        
        self.logger.info(loginRequest)
                
        
        if self.auth_player(loginRequest.accountName, loginRequest.password):
            self.login_player(loginRequest.nextLoginKey)
        else:
            LoginDeniedPktOut.send(self.transport, 
                                   LoginDeniedReason.IncorrectNamePassword)
                                    
        
    def recv_0xa4(self):        
        if len(self.data) < 149: return
        self.data = self.data[149:]
        
        
    def recv_0xa0(self):        
        try:
            (server_index,) = SelectServerPktIn._unpack(self.data)
        except struct.error:            
            return
        else:
            self.data = self.data[LoginRequestPktIn.size:]        
        self.logger.info('selected server with idx %d: %s', server_index, self.servers[server_index]['server name'])
                
                                                    
    @property
    def numClients(self):
        return self.factory._num_clients
    
    @numClients.setter
    def numClients(self, value):
        self.factory._num_clients = value
                                        

class UOProtoFactory(ServerFactory):            
    def __init__(self):        
        self._num_clients = 0        
        
    def buildProtocol(self, addr):
        p = UOProtoDemultiplexer(self)
        p.set_logger(uoserv_log)
        return p
    
    
# -- server quick bootstrap --      
    
def _listen_cb(endpoint):            
    port = endpoint.listen(UOProtoFactory())
    port.addErrback(_on_listen_error)
    port.addCallback(_on_listen_success) 
                
        
def _on_listen_success(result):
    ip_address = get_ip()
        
    if result is not None:
        uoserv_log.info("Listening at %s:%d" % (ip_address, result.port))
    

def _on_listen_error(reason):
    if reason.type is CannotListenError:
        uoserv_log.critical("Can't bind to port %d!", PORT)
        uoserv_log.debug(reason.getErrorMessage())
    else:
        uoserv_log.critical(reason.getErrorMessage())
    reactor.stop()  # @UndefinedVariable    

# --


def main(argv):  # @UnusedVariable
    global accounts
    uoserv_log.info("Starting uoserv v{UOSERV_VER} ..".format(**globals()))
    uoserv_log.info('Loading accounts.yaml ..')
    accounts = yaml.load(open(os.path.join(SERVER_DIR, WORLD_SAVE_DIR, 'accounts.yaml')))    
    accounts = {acct.name.lower():acct for acct in accounts}
                    
    endpoint = TCP4ServerEndpoint(reactor, PORT)                  
    reactor.callWhenRunning(_listen_cb, endpoint)    # @UndefinedVariable
    reactor.run()  # @UndefinedVariable        
    return 0

    
if __name__ == '__main__':
    sys.exit(main(sys.argv))
    