from collections import namedtuple
import socket
import struct

import enum

from utils import public


class UOPktMeta(type):
    def __str__(cls):  # @NoSelf
        return cls.__name__

#     def __init__(cls, *args):
#         print args[0]
#         dict_ = args[2]
#         if '__doc__' in dict_:
#             print dict_['__doc__'] 
#         type.__init__(cls, *args)
        

@public
class UOPacket(object):
    __metaclass__ = UOPktMeta    
    getbyte = struct.Struct('B').unpack
    getbyte_from = struct.Struct('B').unpack_from
                
    @classmethod
    def _unpack_ntuple(cls, data):
        packet_list = [s.partition('\x00')[0] if type(s) is str else s for s in cls._unpack(data)]
        return cls._struct._make(packet_list)
       
    @classmethod
    def recv(cls):
        raise NotImplementedError     
        
    @classmethod
    def build(cls, *args):
        return struct.Struct(cls._packet_build).pack(cls.id, *args) 
    
    @classmethod
    def send(cls, transport, *args):
        transport.write(cls.build(*args))
        
        

@public
@enum.unique    
class PacketMsgIn(enum.IntEnum):   
    LoginRequest = 0x80
    ClientSpy = 0xA4
    SelectServer = 0xA0
    
    
@public    
@enum.unique    
class PacketMsgOut(enum.IntEnum):
    LoginDenied = 0x82
    GameServerList = 0xA8    
    ConnectToGameServer = 0x8C
    

@public
class LoginRequestPktIn(UOPacket):
    """
    Size: 62 Bytes
    Packet Build
    BYTE[1] Command
    BYTE[30] Account Name
    BYTE[30] Password
    BYTE[1] NextLoginKey value from uo.cfg on client machine.
    """
    id = PacketMsgIn.LoginRequest
    _packet_build = 'x30s30sB'         
    size = struct.calcsize(_packet_build)            
    _unpack = struct.Struct(_packet_build).unpack    
    _struct = namedtuple('LoginRequest', 'accountName, password, nextLoginKey')


@public
class SelectServerPktIn(UOPacket):
    id = PacketMsgIn.SelectServer
    _packet_build = '!xH'
    _unpack = struct.Struct(_packet_build).unpack
    _struct = namedtuple(PacketMsgIn(id).name, 'shardID')

@public
class ConnectToGameServerPktOut(UOPacket):    
    """
    BYTE[1] cmd
    BYTE[4] gameServer IP
    BYTE[2] gameServer port
    BYTE[4] new key
    """
    
    id = PacketMsgOut.ConnectToGameServer 
    _packet_build = '!BIHI'
    
    
@public    
class LoginDeniedPktOut(UOPacket):    
    """ 
    Packet Name: Login Denied
            
    Packet: 0x82
    Sent By: Server
    Size: 2 Bytes
    
    Packet Build
    BYTE[1] cmd
    BYTE[1] reason
    
    Notes
    Login denied reasons (gump message):
    """        
    id = PacketMsgOut.LoginDenied
    _packet_build = 'BB'
    size = struct.calcsize(_packet_build)


@public    
class GameServerListPktOut(UOPacket):
    """
    Packet Name: Game Server List    
    Packet: 0xA8
    Sent By: Server
    Size: Variable
    
    Packet Build
    BYTE[1] cmd
    BYTE[2] length
    BYTE[1] System Info Flag
    BYTE[2] # of servers
    
    Then each server --
    
        BYTE[2] server index (0-based)
        BYTE[32] server name
        BYTE percent full
        BYTE timezone
        BYTE[4] server ip to ping        
    
    Notes
    System Info Flags: 0xCC - Do not send video card info. 0x64 - Send Video card. RunUO And SteamEngine both send 0x5D.
    
    Server IP has to be sent in reverse order. For example, 192.168.0.1 is sent as 0100A8C0.
    """
    id = PacketMsgOut.GameServerList
    _packet_build = '!BHBH'
    _packet_build_subitem = '!H32sBbI'
    pack = struct.Struct(_packet_build).pack
    _pack_subitem = struct.Struct(_packet_build_subitem).pack
    
    
    @classmethod
    def build(cls, *args):
        raise NotImplementedError()
    
    @classmethod
    def send(cls, transport, servers):                
        packet = cls.pack(
            *[cls.id, 
              len(servers)*struct.calcsize(cls._packet_build_subitem) + struct.calcsize(cls._packet_build), 
              0x5D, 
              len(servers),
              ]
            )
        
        d = transport.getHost ()
                   
        for server_index, server in enumerate(servers):              
            if u'--ip--' in server["server ip"]: 
                server["server ip"] = d.host                
            server["percent full"] = 0

            packet += cls._pack_subitem( server_index, 
                                        server["server name"].encode('latin1'),
                                        server["percent full"],
                                        server["timezone"],
                                        *struct.unpack('<I', socket.inet_aton(server["server ip"]))
                                        )            
        transport.write(packet)        


@public    
@enum.unique    
class LoginDeniedReason(enum.IntEnum):
    (
    IncorrectNamePassword,
    SomeoneUsingAccount,
    AccountBlocked,
    InvalidAccountCredentials, 
    CommunicationProblem,
    ConcurrencyLimitMet,
    TimeLimitMet,
    GeneralAuthenticationFailure,
    ) = range(8)    
    
    
if __name__ == '__main__':
    pass            